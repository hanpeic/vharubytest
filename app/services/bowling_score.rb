class BowlingScore
  attr_reader :score_list, :total_score, :error_message

  def initialize(score_list)
    @score_list = score_list
  end

  def call
    return false unless valid?
    frame = 0
    @total_score = 0
    current_index = 0
    last_throw = 0
    while frame < 10 && score_list[current_index].present?
      # strike
      if score_list[current_index] == 10
        @total_score += 10 + (score_list[current_index + 1] || 0) + (score_list[current_index + 2] || 0)
        last_throw = current_index + 2
        current_index += 1
      # spare
      elsif score_list[current_index] + (score_list[current_index + 1] || 0) == 10
        @total_score += 10 + (score_list[current_index + 2] || 0)
        last_throw = current_index + 2
        current_index += 2
      else
        @total_score += score_list[current_index] + (score_list[current_index + 1] || 0)
        last_throw = current_index + 1
        current_index += 2
      end
      frame += 1
    end
    @error_message = 'Sorry, you throw too many times!' and return false if score_list[last_throw + 1].present?
    true
  end

  private

  def valid?
    @error_message = 'Please enter the separated numbers (ex.: 1 2 3 4)' and return false if score_list.blank?
    numbers = score_list.strip.split(' ')
    @error_message = 'Please enter valid numbers, each number should be between 0 - 10' and return false if numbers.any? { |score| /\A(\d|10)\z/.match(score).blank? }
    @score_list = numbers.map(&:to_i)
    true
  end
end
