class CalculateScoreController < ApplicationController
  def index
  end

  def calculate
    bowling_score = BowlingScore.new(params[:score_list])
    if bowling_score.call
      flash[:notice] = "Input: #{params[:score_list]}, total score: #{bowling_score.total_score}"
    else
      flash[:error] = bowling_score.error_message
    end
    redirect_to action: :index
  end
end
