Ruby Tech Test
=======================

"Ten Pin Bowling Score"

Write a ruby application that takes a string of space separated numbers from 0 to 10
and calculates what that would be as a score in ten pin bowling (hints on how to score in bowling are here: http://bowling2u.com/trivia/game/scoring.asp).

If a final score cannot be determined from the input then method should return the
"current" score (i.e. assumes any remaining bowls score 0).

Example inputs and outputs

* "1 2 3 4" -> 10
* "9 1 9 1" -> 29
* "1 1 1 1 10 1 1" -> 18
* "10 10 10 10 10 10 10 10 10 10 10 10" -> 300

# Usage
1 Install ruby  
2 Download the code: git clone https://tom_han@bitbucket.org/tom_han/vharubytest.git  
3 Run command: bundle install  
4 Start the server: rails s  
5 Open http://localhost:3000 in browser  
6 Enjoy it!  

# Test
Run command: rspec