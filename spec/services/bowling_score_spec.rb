require 'rails_helper'

describe BowlingScore do
  let(:score_list) { '1 2 3 4' }
  subject { described_class.new(score_list) }

  describe '.initialize' do
    it 'assigns correct instance variables' do
      expect(subject.instance_variable_get(:@score_list)).to eq(score_list)
    end
  end

  describe '#call' do
    context 'when the score_list is empty' do
      it 'returns false' do
        score_list = ''
        subject = described_class.new(score_list)
        expect(subject.call).to be_falsey
        expect(subject.error_message).to eq('Please enter the separated numbers (ex.: 1 2 3 4)')
      end
    end

    context 'when the input score is not in 0 - 10' do
      it 'returns false' do
        score_list = '0 a 2'
        subject = described_class.new(score_list)
        expect(subject.call).to be_falsey
        expect(subject.error_message).to eq('Please enter valid numbers, each number should be between 0 - 10')
      end
    end

    context 'when throws too many times' do
      it 'returns false' do
        score_list = '10 10 10 10 10 10 10 10 10 10 10 10 8'
        subject = described_class.new(score_list)
        expect(subject.call).to be_falsey
        expect(subject.error_message).to eq('Sorry, you throw too many times!')
      end
    end

    context 'when the score_list is valid' do
      context 'when the score_list is 1 2 3 4' do
        it 'returns true' do
          score_list = '1 2 3 4'
          subject = described_class.new(score_list)
          expect(subject.call).to be_truthy
          expect(subject.total_score).to eq(10)
        end
      end

      context 'when the score_list is 9 1 9 1' do
        it 'returns true' do
          score_list = '9 1 9 1'
          subject = described_class.new(score_list)
          expect(subject.call).to be_truthy
          expect(subject.total_score).to eq(29)
        end
      end

      context 'when the score_list is 1 1 1 1 10 1 1' do
        it 'returns true' do
          score_list = '1 1 1 1 10 1 1'
          subject = described_class.new(score_list)
          expect(subject.call).to be_truthy
          expect(subject.total_score).to eq(18)
        end
      end

      context 'when the score_list is 10 10 10 10 10 10 10 10 10 10 10 10' do
        it 'returns true' do
          score_list = '10 10 10 10 10 10 10 10 10 10 10 10'
          subject = described_class.new(score_list)
          expect(subject.call).to be_truthy
          expect(subject.total_score).to eq(300)
        end
      end
    end
  end
end
