require 'rails_helper'

describe CalculateScoreController do
  describe 'index' do
    it 'returns http success' do
      get :index

      expect(response).to be_success
      expect(response).to have_http_status(200)
    end
  end

  describe 'calculate' do
    context 'when the params is not valid' do
      it 'returns error message' do
        post :calculate, score_list: '1 11 1'

        expect(flash[:error]).to eq('Please enter valid numbers, each number should be between 0 - 10')
        expect(response).to redirect_to root_path
      end
    end

    context 'when the params is valid' do
      it 'returns total score' do
        post :calculate, score_list: '10 10 10 10 10 10 10 10 10 10 10 10'

        expect(flash[:notice]).to eq('Input: 10 10 10 10 10 10 10 10 10 10 10 10, total score: 300')
        expect(response).to redirect_to root_path
      end
    end
  end
end
